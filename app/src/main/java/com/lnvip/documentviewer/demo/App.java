package com.lnvip.documentviewer.demo;

import android.app.Application;

import com.lnvip.documentviewer.DocumentViewer;

/**
 * @author MartinKent
 * @time 2018/1/10
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DocumentViewer.init(this);
    }
}

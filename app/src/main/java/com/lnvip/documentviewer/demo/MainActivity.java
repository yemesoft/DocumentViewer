package com.lnvip.documentviewer.demo;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lnvip.android.permissions.RequestPermissions;
import com.lnvip.android.permissions.TipMode;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.ValueCallback;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button button = new Button(this);
        button.setText("Open file");
        setContentView(button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();
            }
        });
    }

    @RequestPermissions(value = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.SYSTEM_ALERT_WINDOW
    }, tipMode = TipMode.Dialog)
    private void openFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, 0x7003);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (0x7003 == requestCode) {
            if (RESULT_OK == resultCode) {
                Uri uri = data.getData(); // 获取用户选择文件的URI
                // 通过ContentProvider查询文件路径
                ContentResolver resolver = this.getContentResolver();
                Cursor cursor = resolver.query(uri, null, null, null, null);
                if (cursor == null) {
                    // 未查询到，说明为普通文件，可直接通过URI获取文件路径
                    String path = uri.getPath();
                    open(path);
                    return;
                }
                if (cursor.moveToFirst()) {
                    // 多媒体文件，从数据库中获取文件的真实路径
                    String filePath = cursor.getString(cursor.getColumnIndex("_data"));
                    open(filePath);
                }
                cursor.close();
            }
        }
    }

    private void open(String filePath) {
        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("style", "1");
//        params.put("local", "true");
//        params.put("memuData", "{}");

        int result = QbSdk.openFileReader(this, filePath, params, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                System.out.println(s);
            }
        });
        System.out.println("result=" + result);
    }
}
